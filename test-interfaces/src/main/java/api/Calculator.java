package api;

/**
 * This is for testing plugins that implement
 * interfaces
 * */
public interface Calculator {
    int add(int a, int b);
    int subtract(int a, int b);
    int multiply(int a, int b);
    int divide(int a, int b);
}
