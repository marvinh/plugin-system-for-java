package de.osshangar.plugin;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/de/osshangar/plugin/features")
public class RunCucumberTests {
}
