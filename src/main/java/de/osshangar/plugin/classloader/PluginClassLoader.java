package de.osshangar.plugin.classloader;

import lombok.Getter;
import lombok.NonNull;

import java.util.Map;

/**
 * Class loader that stores byte code of known classes in RAM
 * */
public class PluginClassLoader extends ClassLoader {
    // contains all class files that are known to the class loader with their name
    @Getter
    private final Map<String, byte[]> byteCodes;

    /**
     * Creates the class loader
     * @param classesFiles Map of full qualified class names as keys and the byte code of the classes as values
     */
    public PluginClassLoader(@NonNull Map<String, byte[]> classesFiles) {
        super(PluginClassLoader.class.getClassLoader());
        this.byteCodes = classesFiles;
    }

    @Override
    public Class<?> findClass(@NonNull String fullQualifiedClassName) throws ClassNotFoundException {
        if (!byteCodes.containsKey(fullQualifiedClassName)){
            throw new ClassNotFoundException(String.format("Did not find class '%s'", fullQualifiedClassName));
        }
        return defineClass(fullQualifiedClassName, byteCodes.get(fullQualifiedClassName), 0, byteCodes.get(fullQualifiedClassName).length);
    }

    @Override
    public Class<?> loadClass(@NonNull String fullQualifiedClassName) throws ClassNotFoundException {
        if (byteCodes.containsKey(fullQualifiedClassName)){
            return findClass(fullQualifiedClassName);
        }
        ClassLoader defaultLoader = Thread.currentThread().getContextClassLoader();
        return defaultLoader.loadClass(fullQualifiedClassName);
    }

}
