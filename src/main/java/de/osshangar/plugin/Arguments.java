package de.osshangar.plugin;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@Builder
public class Arguments {
    /**This is the signature of the constructor
     * to call while the instantiation */
    @NonNull
    private final Class<?>[] constructor;

    /**This are the arguments that should be provided
     * to the constructor. The order of the arguments
     * in the provided array must match the constructor
     * signature.
     * */
    @NonNull
    private final Object[] arguments;
}
